'use strict'

module.exports = {
  rules: {
    'no-undef-init': 'error',
    'no-undefined': 'warn', // use typeof check instead of ===, and null instead of undefined where possible.
    'no-use-before-define': ['error', 'nofunc'],
  },
}
