'use strict'

module.exports = {
  rules: {
    'callback-return': ['warn', [
      'callback',
      'cb',
      'done',
    ]],
    'handle-callback-err': ['warn', '(err|error)'],
  },
}
