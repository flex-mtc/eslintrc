'use strict'

module.exports = {
  plugins: ['jsdoc'],
  rules: {
    'jsdoc/check-access': 'error',
    'jsdoc/check-alignment': 'error',
    'jsdoc/check-param-names': 'error',
    'jsdoc/check-property-names': 'error',
    'jsdoc/check-syntax': 'error',
    'jsdoc/check-tag-names': 'error',
    'jsdoc/check-types': 'error',
    'jsdoc/check-values': 'error',
    'jsdoc/empty-tags': 'warn',
    'jsdoc/implements-on-classes': 'warn',
    'jsdoc/no-undefined-types': 'warn',
    'jsdoc/require-param-description': 'warn',
    'jsdoc/require-param-name': 'error',
    'jsdoc/require-param-type': 'error',
    'jsdoc/require-property': 'warn',
    'jsdoc/require-property-description': 'warn',
    'jsdoc/require-property-name': 'error',
    'jsdoc/require-property-type': 'error',
    'jsdoc/require-returns-check': 'error',
    'jsdoc/require-returns-description': 'warn',
    'jsdoc/require-returns-type': 'error',
    'jsdoc/valid-types': 'error',
  },
  settings: {
    jsdoc: {
      tagNamePreference: {
        augments: 'extends',
      },
      preferredTypes: {
        '.<>': '<>',
      },
    },
  },
}
