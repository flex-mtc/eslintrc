'use strict'

module.exports = {
  rules: {
    'accessor-pairs': 'warn',
    'array-callback-return': 'warn',
    'block-scoped-var': 'warn',
    'consistent-return': 'warn',
    'curly': ['error', 'all'],
    'default-case': 'error', // add an empty default case to resolve
    'dot-location': ['error', 'property'],
    'dot-notation': 'error',
    'eqeqeq': ['warn', 'smart'],
    'grouped-accessor-pairs': ['error', 'setBeforeGet'],
    'guard-for-in': 'warn',
    'no-alert': 'error',
    'no-caller': 'error',
    'no-constructor-return': 'error',
    'no-div-regex': 'warn', // enclose = in square brackets, e.g. /[=]regex/.test('')
    'no-empty-function': 'error', // comment "do nothing" to disable
    'no-eq-null': 'warn',
    'no-eval': 'error', // requires justification and proper sanitization for case-by-case waivering
    'no-extend-native': 'error', // disable separately for polyfills
    'no-extra-bind': 'error',
    'no-floating-decimal': 'error', // use full notation, e.g. 0.5 and 2.0 instead of .5 and 2.
    'no-implicit-coercion': 'error', // use proper type casting
    'no-implied-eval': 'error', // see no-eval
    'no-invalid-this': 'error',
    'no-iterator': 'error',
    'no-labels': 'error',
    'no-loop-func': 'warn',
    'no-new': 'error',
    'no-new-func': 'error',
    'no-new-wrappers': 'error',
    'no-octal-escape': 'error',
    'no-return-assign': 'warn', // enclose in parentheses
    'no-self-compare': 'error',
    'no-sequences': 'error',
    'no-throw-literal': 'error', // throw new Error(msg) instead
    'no-unmodified-loop-condition': 'warn',
    'no-unused-expressions': 'error',
    'no-useless-call': 'error',
    'no-useless-concat': 'warn',
    'no-useless-return': 'warn',
    'no-void': 'error', // use undefined instead
    'no-warning-comments': ['warn', {
      terms: [
        'todo',
      ],
      location: 'start',
    }],
    'radix': ['error', 'as-needed'],
    'wrap-iife': ['error', 'inside', {
      functionPrototypeMethods: true,
    }],
    'yoda': ['error', 'never', {
      exceptRange: true,
    }],
  },
}
