'use strict'

module.exports = {
  rules: {
    'no-dupe-else-if': 'error',
    'no-extra-parens': ['warn', 'all', {
      conditionalAssign: false,
      returnAssign: false,
      nestedBinaryExpressions: false,
    }],
    'no-setter-return': 'error',
  },
}
