'use strict'

module.exports = {
  rules: {
    'array-bracket-newline': ['error', 'consistent'],
    'array-bracket-spacing': ['error', 'never'],
    'array-element-newline': ['error', 'consistent'],
    'brace-style': ['error', '1tbs'],
    'camelcase': 'error',
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      functions: 'never',
    }],
    'comma-spacing': ['error', {
      before: false,
      after: true,
    }],
    'comma-style': ['error', 'last'],
    'computed-property-spacing': ['error', 'never'],
    'eol-last': ['error', 'always'],
    'func-call-spacing': ['error', 'never'],
    'func-names': ['error', 'as-needed'],
    'function-call-argument-newline': ['error', 'consistent'],
    'function-paren-newline': ['error', 'consistent'],
    'indent': ['error', 2],
    'key-spacing': ['error', {
      singleLine: {
        beforeColon: false,
        afterColon: true,
        mode: 'strict',
      },
      multiLine: {
        beforeColon: false,
        afterColon: true,
        mode: 'minimum',
      },
    }],
    'keyword-spacing': ['error', {
      before: true,
      after: true,
    }],
    'linebreak-style': ['error', 'unix'],
    'max-depth': ['warn', {max: 5}],
    'max-len': ['warn', {
      code: 120,
      tabWidth: 2,
    }],
    'max-nested-callbacks': ['warn', {max: 5}],
    'max-params': ['warn', {max: 5}],
    'max-statements-per-line': ['error', {max: 1}],
    'multiline-ternary': ['error', 'always-multiline'],
    'new-parens': ['error', 'always'],
    'newline-per-chained-call': ['warn', {
      ignoreChainWithDepth: 3,
    }],
    'no-array-constructor': 'error',
    'no-multi-assign': 'error',
    'no-negated-condition': 'warn',
    'no-new-object': 'error',
    'no-trailing-spaces': ['error', {
      skipBlankLines: true,
    }],
    'no-unneeded-ternary': ['error', {
      defaultAssignment: false,
    }],
    'no-whitespace-before-property': 'error',
    'object-curly-newline': ['error', {consistent: true}],
    'object-curly-spacing': ['error', 'never'],
    'object-property-newline': ['error', {
      allowAllPropertiesOnSameLine: true,
    }],
    'one-var': ['error', {
      uninitialized: 'consecutive',
    }],
    'operator-linebreak': ['error', 'before'],
    'quote-props': ['error', 'as-needed', {
      unnecessary: false, // allow unnecessary quotes for consistency
    }],
    'quotes': ['error', 'single'],
    'semi': ['error', 'never', {
      beforeStatementContinuationChars: 'always',
    }],
    'semi-spacing': ['error', {
      before: false,
      after: false,
    }],
    'semi-style': ['error', 'first'],
    'space-before-blocks': ['error', 'always'],
    'space-before-function-paren': ['error', {
      anonymous: 'never',
      named: 'never',
      asyncArrow: 'always',
    }],
    'space-in-parens': ['error', 'never'],
    'space-infix-ops': 'error',
    'space-unary-ops': ['error', {
      words: true,
      nonwords: false,
    }],
    'spaced-comment': ['warn', 'always', {
      line: {
        markers: [
          '/',
        ],
        exceptions: [
          '/',
          '-',
          '=',
          '+',
        ],
      },
      block: {
        markers: [
          '*',
          '!',
        ],
        exceptions: [
          '*',
          '-',
          '=',
        ],
        balanced: true,
      },
    }],
    'switch-colon-spacing': ['error', {
      before: false,
      after: true,
    }],
    'unicode-bom': ['error', 'never'],
  },
}
