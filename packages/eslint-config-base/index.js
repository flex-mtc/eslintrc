'use strict'

module.exports = {
  plugins: [
    ...require('./rules/plugin-jsdoc').plugins,
  ],
  extends: [
    'eslint:recommended',
  ],
  reportUnusedDisableDirectives: true,
  rules: {
    ...require('./rules/possible-errors').rules,
    ...require('./rules/best-practices').rules,
    ...require('./rules/strict-mode').rules,
    ...require('./rules/variables').rules,
    ...require('./rules/nodejs-and-commonjs').rules,
    ...require('./rules/stylistic-issues').rules,
    ...require('./rules/plugin-jsdoc').rules,
  },
  settings: {
    ...require('./rules/plugin-jsdoc').settings,
  },
}
