'use strict'

module.exports = {
  extends: [
    require.resolve('@flex-mtc/eslint-config-browser'),
  ],
  env: {
    jquery: true,
  },
  globals: {
    sap: 'readonly',
    jQuery: 'readonly',
  },
  rules: {
    /**************************
     * Stylistic Issues
     **************************/
    'consistent-this': ['warn', 'that'],
    'func-style': ['error', 'expression'],
    'new-cap': 'warn',
  },
}
