'use strict'

module.exports = {
  rules: {
    'class-methods-use-this': 'warn',
    'max-classes-per-file': ['warn', 1],
    'no-return-await': 'error',
    'require-await': 'warn',
    'require-unicode-regexp': 'warn',
  },
}
