'use strict'

module.exports = {
  rules: {
    'no-await-in-loop': 'warn',
    'no-template-curly-in-string': 'warn',
    'require-atomic-updates': 'warn',
  },
}
