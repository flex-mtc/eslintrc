'use strict'

module.exports = {
  rules: {
    'callback-return': ['warn', [
      'callback',
      'cb',
      'done',
      'resolve',
      'reject',
    ]],
  },
}
