'use strict'

module.exports = {
  rules: {
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      functions: 'only-multiline',
    }],
    'computed-property-spacing': ['error', 'never', {
      enforceForClassMembers: true,
    }],
    'template-tag-spacing': ['error', 'never'],
  },
}
