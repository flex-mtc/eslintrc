'use strict'

module.exports = {
  rules: {
    'arrow-body-style': ['error', 'as-needed'],
    'arrow-parens': ['warn', 'as-needed'],
    'arrow-spacing': ['error', {
      before: true,
      after: true,
    }],
    'generator-star-spacing': ['error', {
      named: 'after',
      anonymous: 'neither',
      method: 'before',
    }],
    'no-confusing-arrow': ['warn', {
      allowParens: true,
    }],
    'no-duplicate-imports': ['error', {
      includeExports: true, // import to variable and then export
    }],
    'no-useless-computed-key': 'error',
    'no-useless-constructor': 'error',
    'no-useless-rename': 'error',
    'no-var': 'error', // use let or const
    'object-shorthand': ['error', 'consistent-as-needed'],
    'prefer-arrow-callback': 'warn',
    'prefer-numeric-literals': 'error',
    'rest-spread-spacing': ['error', 'never'],
    'symbol-description': 'error',
    'template-curly-spacing': ['error', 'never'],
    'yield-star-spacing': ['error', 'after'],
  },
}
