'use strict'

module.exports = {
  plugins: [
    ...require('./rules/plugin-promise').plugins,
  ],
  extends: [
    require.resolve('@flex-mtc/eslint-config-base'),
    ...require('./rules/plugin-promise').extends,
  ],
  env: {
    es6: true,
    es2017: true,
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    ...require('./rules/possible-errors').rules,
    ...require('./rules/best-practices').rules,
    ...require('./rules/nodejs-and-commonjs').rules,
    ...require('./rules/stylistic-issues').rules,
    ...require('./rules/ecmascript-6').rules,
    ...require('./rules/plugin-promise').rules,
  },
}
