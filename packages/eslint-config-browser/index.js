'use strict'

module.exports = {
  plugins: [
    'compat',
  ],
  extends: [
    require.resolve('@flex-mtc/eslint-config-es6'),
    'plugin:compat/recommended',
  ],
  env: {
    browser: true,
  },
  rules: {
    /**************************
     * Possible Errors
     **************************/
    'no-console': 'error',
    /**************************
     * Best Practices
     **************************/
    'no-implicit-globals': ['error', {
      lexicalBindings: true,
    }],
    'no-script-url': 'error',
    /**************************
     * Stylistic Issues
     **************************/
    'indent': ['error', 'tab'],
    'quotes': ['error', 'double'],
  },
}
