'use strict'

module.exports = {
  plugins: [
    ...require('./rules/plugin-node').plugins,
  ],
  extends: [
    require.resolve('@flex-mtc/eslint-config-es6'),
    ...require('./rules/plugin-node').extends,
  ],
  env: {
    node: true,
  },
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    ...require('./rules/nodejs-and-commonjs').rules,
    ...require('./rules/stylistic-issues').rules,
    ...require('./rules/plugin-node').rules,
  },
}
