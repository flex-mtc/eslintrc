'use strict'

module.exports = {
  rules: {
    'func-style': ['error', 'declaration'],
    'max-lines': ['warn', {max: 360}],
    'max-statements': ['warn', {max: 100}],
  },
}
