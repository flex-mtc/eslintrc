'use strict'

module.exports = {
  plugins: ['node'],
  extends: ['plugin:node/recommended'],
  rules: {
    /**************************
     * Stylistic Issues
     **************************/
    'node/file-extension-in-import': ['error', 'always', {
      '.js': 'never',
    }],
    'node/prefer-global/buffer': 'error',
    'node/prefer-global/console': 'error',
    'node/prefer-global/process': 'error',
    'node/prefer-global/text-decoder': 'error',
    'node/prefer-global/text-encoder': 'error',
    'node/prefer-global/url-search-params': 'error',
    'node/prefer-global/url': 'error',
  },
}
