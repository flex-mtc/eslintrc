'use strict'

module.exports = {
  rules: {
    'callback-return': ['warn', [
      'callback',
      'cb',
      'done',
      'next',
      'resolve',
      'reject',
    ]],
    'global-require': 'warn',
    'no-buffer-constructor': 'error',
    'no-mixed-requires': ['error', {
      grouping: true,
    }],
    'no-new-require': 'error',
    'no-path-concat': 'error',
    'no-sync': 'warn',
  },
}
